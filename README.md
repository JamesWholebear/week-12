# Week 12 Readme (useState, npm modules)

- To explore App and Prelim, toggle the comments in `/src/index.js`

## Preliminary Things
- Destructuring (not important, just enough to not be confused and be able to use it for hooks)
- Speading

Found in:
- /src/Prelim.js

## useState

Found in:
- /src/Prelim.js
- /src/App.js

## libraries like react-hook-form

Found in:
- /src/App.js

## assignment:

- Make an Imperial or Jedi themed todo list (be creative)
  - Just one input for the task "name" is enough
  - Use react-hook-form (`yarn add react-hook-form` or `npm install react-hook-form`) 
  - Be sure to restart your react-app dev server after this step or it won't find the new library
  - `import { useForm } from "react-hook-form";` at the top of `App.js`
- Add a modal (some kind of window that pops up using state)
- Allow filtering (checkboxes or buttons that show the list filtered in some way)
- Allow editing
- Some way of completing a task
- Some way of deleting/clearing tasks
- Some relevant background image

- stretch goals: 
  - reordering (this one can be tough)
  - use multiple inputs and give each task different data points
  - track "progress" of completion somehow
  - add another npm module or two like https://www.npmjs.com/package/react-hamburger-menu
    - incorporate into your modal
  - animate your modal
