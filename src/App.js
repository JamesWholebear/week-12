import { useState, Fragment } from "react";
import './App.css';
import { useForm } from "react-hook-form";

import ship from "./ship.png";
import red from "./red.png";
import blue from "./blue.png";
import purple from "./purple.png";


const App = () => {
  const test = useState();
  console.log(test);

  return (
    <div className="embiggen">
      Check the console
    </div>
  );
}


// ready for a button? See next "App"
































// const App = () => {
//   const [open, setOpen] = useState(false);
// 
//   // setOpen(!open); // very very very very bad
// 
//   return (
//     <div className="embiggen">
//       <button onClick={() => setOpen(!open)}>Toggle</button>
//       {open && <div>Men is open!</div> /*break down what's happening here*/}
//     </div>
//   );
// }















// 
// const characters = {
//   Suzie: red,
//   Jim: blue,
//   King: purple
// }
// const characterList = Object.keys(characters);
// 
// const App = () => {
//   const [imposter, setImposter] = useState();
//   const [onMission, setOnMission] = useState();
//   const [winner, setWinner] = useState();
// 
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={ship} className={`App-logo ${onMission ? "fast-spin" : ""}`} alt="logo" />
//       </header>
//       <h3>Crew:</h3>
//       {characterList.map(character => (
//         <span key={character}>
//           <img 
//             alt="crewmate" 
//             src={characters[character]} 
//           // onClick={}
//           />
//           {character}
//         </span>
//       ))}
//       {imposter && (
//         <div>
//           <div>
//             <h3>Imposter: {imposter}</h3>
//             <img alt="crewmate" src={characters[imposter]} onClick={() => setImposter()}  />
//           </div>
//           <div>
//             <button
//              // onClick={}
//             >
//               Send on mission
//             </button>
//           </div>
//         </div>
//       )}
//       {winner && <h3>Winner: {winner}</h3>}
//       {onMission && "Crew is attempting tasks..."}
//     </div>
//   );
// }














//   const sendOnMission = () => {
//     setOnMission(true);
//     setTimeout(() => {
//       setOnMission(false);
//       setImposter();
// 
//       setWinner(getRandomInt(1, 2) % 2 ? imposter : "Crewmates")
//     }, [2000]);
//   }
// 
//   const selectImposter = character => {
//     setImposter(character);
//     setWinner();
//   }



const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max + 1);
  return Math.floor(Math.random() * (max - min) + min);
}

// inputs section





// const App = () => {
//   const [adding, setAdding] = useState();
//   const [crew, setCrew] = useState([]);
//   const [name, setName] = useState("");
//   const [age, setAge] = useState("");
//   const [color, setColor] = useState("");
// 
//   // this only fires if your input data passes validation
//   const onSubmit = () => {
//     console.log(name, age, color);
//   };
// 
//   const cancel = () => {
//     console.log(name, age, color);
//   }
// 
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={ship} className="App-logo" alt="logo" />
//       </header>
//       <h3>Crew: {!adding && (<button onClick={() => setAdding(true)}>Add Crew Member</button>)}</h3>
//       {adding ? (
//         <>
//           <div>
//             <label htmlFor="name">
//               <h3>Name</h3>
//               <input value={name} onChange={event => setName(event.target.value)} />
//             </label>
//           </div>    
//           <div>
//             <label htmlFor="age">
//               <h3>Age</h3>
//               <input value={age} onChange={event => setAge(event.target.value)} />
//             </label>
//           </div>    
//           <div>
//             <label htmlFor="color">
//               <h3>Color</h3>
//               <input value={color} onChange={event => setColor(event.target.value)} />
//             </label>
//           </div>
//           <input type="submit" onClick={onSubmit} />
//           <button type="button" onClick={cancel}>Cancel</button>
//         </>
//       ) : (
//         crew.map((character, index) => (
//           <Fragment key={character.name}>
//             <div>
//               <p>Name: {character.name}</p>
//               {character.age && (<p>Age: {character.age}</p>)}
//               <p>Color: {character.color}</p>
//             </div>
//             {index < crew.length - 1 && <div className="divider-bar" />}
//           </Fragment>
//         ))
//       )}
//     </div>
//   );
// }








// const App = () => {
//   const [adding, setAdding] = useState();
//   const [crew, setCrew] = useState([]);
//   const { register, handleSubmit } = useForm();
//   // const { register, handleSubmit, watch, formState: { errors }, reset } = useForm();
// 
//   const onSubmit = newCrewMember => console.log(newCrewMember);
// 
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={ship} className="App-logo" alt="logo" />
//       </header>
//       <h3>Crew: {!adding && (<button onClick={() => setAdding(true)}>Add Crew Member</button>)}</h3>
//       {adding ? (
//         /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
//         <form onSubmit={handleSubmit(onSubmit)}>
//           {/* register your input into the hook by invoking the "register" function */}
//           {/* <input {...register("name", { required: true })} /> */}
//           {/* <input {...register("age")} /> */}
//           {/* <input {...register("color", { required: true })} /> */}
//           <div>
//             <label htmlFor="name">
//               <h3>Name</h3>
//               <input {...register("name", {required: true})} />
//             </label>
//           </div>    
//           <div>
//             <label htmlFor="age">
//               <h3>Age</h3>
//               <input {...register("age")} />
//             </label>
//           </div>    
//           <div>
//             <label htmlFor="color">
//               <h3>Color</h3>
//               <input {...register("color", {required: true})} />
//             </label>
//           </div>
//           <input type="submit" />
//           <button 
//             type="button" 
//             // onClick={cancel}
//           >
//             Cancel
//           </button>
//         </form>
//       ) : (
//         crew.map((character, index) => (
//           <>
//             <div key={character.name}>
//               <p>Name: {character.name}</p>
//               {character.age && (<p>Age: {character.age}</p>)}
//               <p>Color: {character.color}</p>
//             </div>
//             {index < crew.length - 1 && <div className="divider-bar" />}
//           </>
//         ))
//       )}
//     </div>
//   );
// }

// // complete form (for reference)

// const App = () => {
//   const [adding, setAdding] = useState();
//   const [crew, setCrew] = useState([]);
//   const { register, handleSubmit, watch, formState: { errors }, reset } = useForm();
//   console.log(watch("age")); // watch input value by passing the name of it
//   // errors will return when field validation fails
//   console.log(errors);
//   // this only fires if your input data passes validation
//   const onSubmit = newCrewMember => console.log(newCrewMember);
// 
//   const onSubmit2 = (newCrewMember, event) => {
//     setCrew([...crew, newCrewMember])
//     setAdding(false);
//     reset();
//   }
// 
//   const cancel = () => {
//     reset(); // clears all inputs
//     setAdding(false);
//   }
// 
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={ship} className="App-logo" alt="logo" />
//       </header>
//       <h3>Crew: {!adding && (<button onClick={() => setAdding(true)}>Add Crew Member</button>)}</h3>
//       {adding ? (
//         /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
//         <form onSubmit={handleSubmit(onSubmit2)}>
//           {/* register your input into the hook by invoking the "register" function */}
//           {/* <input {...register("name", { required: true })} /> */}
//           {/* <input {...register("age")} /> */}
//           {/* <input {...register("color", { required: true })} /> */}
//           <div>
//             <label htmlFor="name">
//               <h3>Name</h3>
//               <input {...register("name", {required: true})} />
//             </label>
//             {errors.name && <div>This field is required</div>}
//           </div>    
//           <div>
//             <label htmlFor="age">
//               <h3>Age</h3>
//               <input {...register("age")} />
//             </label>
//           </div>    
//           <div>
//             <label htmlFor="color">
//               <h3>Color</h3>
//               <input {...register("color", {required: true})} />
//             </label>
//             {errors.color && <div>This field is required</div>}
//           </div>
//           <input type="submit" />
//           <button type="button" onClick={cancel}>Cancel</button>
//         </form>
//       ) : (
//         crew.map((character, index) => (
//           <>
//             <div key={character.name}>
//               <p>Name: {character.name}</p>
//               {character.age && (<p>Age: {character.age}</p>)}
//               <p>Color: {character.color}</p>
//             </div>
//             {index < crew.length - 1 && <div className="divider-bar" />}
//           </>
//         ))
//       )}
//     </div>
//   );
// }



// <Input /> and <InputCleanerImplementation /> are the same:
const Input = props => {
  const { name, register, required, error, type } = props;
  
  return (
    <div>
      <label htmlFor={name}>
        <h3>{name}</h3>
        <input {...register(name, {required: required})} type={type} />
        {error && <div>This field is required</div>}        
      </label>
    </div>
  )
};

const InputCleanerImplementation = ({ name, register, required, error, type }) => (
  <div>
    <label htmlFor={name}>
      <h3>{name}</h3>
      <input {...register(name, {required: required})} type={type} />
      {error && <div>This field is required</div>}        
    </label>
  </div>
);


// // complete and refactored form app
// const App = () => {
//   const [adding, setAdding] = useState();
//   const [crew, setCrew] = useState([]);
//   const { register, handleSubmit, watch, formState: { errors }, reset } = useForm();
// 
//   const onSubmit = (newCrewMember, event) => {
//     setCrew([...crew, newCrewMember])
//     setAdding(false);
//     reset();
//   }
// 
//   const cancel = () => {
//     reset(); // clears all inputs
//     setAdding(false);
//   }
// 
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={ship} className="App-logo" alt="logo" />
//       </header>
//       <h3>Crew: {!adding && (<button onClick={() => setAdding(true)}>Add Crew Member</button>)}</h3>
//       {adding ? (
//         <form onSubmit={handleSubmit(onSubmit)}>
//           <Input error={errors.name} register={register} name="name" required={true} />
//           <Input register={register} name="age" type="number" />
//           <Input error={errors.color} register={register} name="color" required={true} />
//           <input type="submit" />
//           <button type="button" onClick={cancel}>Cancel</button>
//         </form>
//       ) : (
//         crew.map((character, index) => (
//           <Fragment key={character.name}>
//             <div>
//               <p>Name: {character.name}</p>
//               {character.age && (<p>Age: {character.age}</p>)}
//               <p>Color: {character.color}</p>
//             </div>
//             {index < crew.length - 1 && <div className="divider-bar" />}
//           </Fragment>
//         ))
//       )}
//     </div>
//   );
// }


export default App;