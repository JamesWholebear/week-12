import './App.css';

// part 1: Before we get to hooks
// part 1a: Destructuring arrays
const Prelim = () => {
  const falconCrew = ["Luke", "Leia", "Han", "Chewy", "Obi-wan"];

  const theFalcon = {
    model: "Corellian YT-1300f light freighter",
    pilot: "Han Solo",
    isPieceOfJunk: true
  };

//   console.log(falconCrew);
// 
//   console.log(falconCrew[0])
//   console.log(falconCrew[1]);
// 
//   const first = falconCrew[0];
//   const second = falconCrew[1];
//   console.log(first);
//   console.log(second);
// 
//   const [firstMember, secondMember] = falconCrew;
//   console.log(firstMember);
//   console.log(secondMember);





  // part 1b: destructuring objects

//   if (theFalcon.isPieceOfJunk) console.log("Discount pricing");
// 
//     console.log(theFalcon);
//     const { pilot } = theFalcon;
//     console.log(pilot);




  // part 1c: spreading arrays and objects

    // const newCrew = [...falconCrew];
    // console.log(newCrew);
    // const newCrew2 = [...falconCrew, "Lando"];
    // console.log(newCrew2);




  // part 2: what happens when we want to change something in one of our components?
  // Preserve across renders (what is a render? What causes a rerender?)
// 
//   let count = 0;
// 
//   const increment = () => {
//     console.log(count);
//     count++
//     console.log(count)
//   }

  return (
    <div className="App">
      <header className="App-header">
        <Decode data={falconCrew} />
        <Decode data={theFalcon} />
        {/* <button className="pretty-button" onClick={() => count++}>Count: {count}</button> */}
        {/* <button className="pretty-button" onClick={increment}>Count: {count}</button> */}
      </header>
      <div className="embiggen">
        Check the console
      </div>        
    </div>
  );
}

export default Prelim;

const Decode = props => <pre>{JSON.stringify(props.data)}</pre>

// there's several reasons this won't work, but here is a starter function that gives you an idea

const preserveSomethingAcrossRenders = (defaultValue) => {
  let someReferenceToMaybeWindowOrSomething = defaultValue;

  const changeValue = newValue => {
    someReferenceToMaybeWindowOrSomething = newValue;
  }

  return [someReferenceToMaybeWindowOrSomething, changeValue];
};

// problems:
// need to increment somehow and doing this across renders just doesn't really work
// also when other components reuse this, their "increments" would need to be mindful of one another (this sounds horrific to manage)
// please dont just do global

// Something to keep in mind. Since our app is comprised _completely_ of components, all of the app
// behavior needs to be dictated by those components as well. There's no "app level logic", there's
// only "Component level logic". If you want logic to extend to the entire app, keep that at the top
// of the app, but be careful not to let the entire app be concerned with only what matters to a
// small piece of your app, or rather, something component level lower down in the app tree.
